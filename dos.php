<?php 
function fn2($array, $sortCriterion) {
	$keys = array_column($array, 'scoring');
	$sort = $sortCriterion['scoring'] == 'ASC'? SORT_ASC: SORT_DESC;
		array_multisort($keys, $sort, $array);
	$keys = array_column($array, 'age');
	$sort = $sortCriterion['age'] == 'ASC'? SORT_ASC: SORT_DESC;
		array_multisort($keys, $sort, $array);
	
		return array_reverse($array);
}

$array = [ 
 ['user' => 'Oscar', 'age' => 18, 'scoring' => 40], 
 ['user' => 'Mario', 'age' => 45, 'scoring' => 10], 
 ['user' => 'Zulueta', 'age' => 33, 'scoring' => -78],  
 ['user' => 'Mario', 'age' => 45, 'scoring' => 78], 
 ['user' => 'Patricio', 'age' => 22, 'scoring' => 9], 
]; 
$sortCriterion = ['age' => 'DESC', 'scoring' => 'DESC']; 
$result = fn2($array, $sortCriterion); 

var_dump($result);
?>
